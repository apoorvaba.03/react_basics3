import React from 'react';
import ReactDom from 'react';

class Clock extends React.Component{
constructor(props){
    super(props);
    this.state =
    {
        date:new Date()
    };
}
tick(){
    this.setState({data:
        new Date()});
}
componentDidMount(){
    this.timerID = 
        setInterval(
            () => this.tick(),
            1000

        );
}
componenetWillUnmount(){
    clearInterval(this.timerID);

}
render(){
    return(
        <div>
            <h1>Eudeka React js </h1>
            <h2>It is {this.state.date.toLocaleTimeString()}</h2>
        </div>
    );
}


}
export default Clock;